//
//  ActivarViewController.h
//  TuTurnoApp
//
//  Created by Julián Rúa on 17/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivarViewController : UIViewController
- (IBAction)btnregistrarse:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtcodigo;
- (IBAction)EditingBegintxtcodigo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbmensaje;
@property (weak, nonatomic) IBOutlet UIButton *btnok;

@end
