//
//  AlarmCell.h
//  TuTurno
//
//  Created by julian david rua pino on 6/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UISwitch *SwitchOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lbname;
- (IBAction)Switch:(id)sender;
@property int Id;
@end
