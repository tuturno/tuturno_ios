//
//  AlarmCell.m
//  TuTurno
//
//  Created by julian david rua pino on 6/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "AlarmCell.h"
#import "NegocioAlarms.h"

@implementation AlarmCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)Switch:(id)sender {
    
    NegocioAlarms *Nalarms = [[NegocioAlarms alloc]init];
    
    if(self.SwitchOutlet.on)
    {
        [Nalarms alarmEnable:self.Id];
    }
    else
    {
        [Nalarms alarmDisable:self.Id];
    }

}
@end
