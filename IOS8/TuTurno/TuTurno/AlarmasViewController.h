//
//  AlarmasViewController.h
//  TuTurno
//
//  Created by julian david rua pino on 5/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmasViewController : UIViewController
{
    NSArray* MiArray;
}
- (IBAction)touchBack:(id)sender;
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
