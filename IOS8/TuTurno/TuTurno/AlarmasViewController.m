//
//  AlarmasViewController.m
//  TuTurno
//
//  Created by julian david rua pino on 5/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "AlarmasViewController.h"
#import "NegocioAlarms.h"
#import "AlarmCell.h"
#import "NegocioTurno.h"

@interface AlarmasViewController ()

@end

@implementation AlarmasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: @"http://webcontrol.tuturnoapp.com/images/santafe-logo-width.png"]];
    // UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageWithData: imageData]];
    
    self.image.image = [UIImage imageWithData:imageData];
    
    [NegocioTurno deactivateNotificatoins];
    
    NegocioAlarms *Nalarm = [[NegocioAlarms alloc]init];
    
     MiArray = Nalarm.getAlarms;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [MiArray count];
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ContactCell";
    
    AlarmCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        [tableView registerNib:[UINib nibWithNibName:@"AlarmCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    
    // NSLog([NSString stringWithFormat:@"%d",indexPath.row]);
    
    NSDictionary *datos = MiArray[indexPath.row];
    
    int minutes = [datos[@"minutesLeft"]intValue];
    Boolean enable = [datos[@"enabled"] boolValue];
    int Id = [datos[@"id"] intValue];
    
    [cell.SwitchOutlet setOn:enable];

    cell.Id = Id;
    
    cell.lbname.text = [NSString stringWithFormat:@"%d minutos antes",minutes];
    
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchBack:(id)sender {
    
    [NegocioTurno activateNotifications];
    [self dismissViewControllerAnimated:YES completion:nil];
   // [self performSegueWithIdentifier:@"alarm-infoturno" sender:self];
}

- (IBAction)btnBack:(id)sender {
    
    [NegocioTurno activateNotifications];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
