//
//  Alert.h
//  TuTurno
//
//  Created by julian david rua pino on 8/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Alert : UIViewController

+(void)mostrar:(NSString*)mensaje;

@end
