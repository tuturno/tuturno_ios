//
//  Alert.m
//  TuTurno
//
//  Created by julian david rua pino on 8/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "Alert.h"

@implementation Alert

+(void)mostrar:(NSString*)mensaje
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TuTurno"
                                                    message:mensaje
                                                delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
