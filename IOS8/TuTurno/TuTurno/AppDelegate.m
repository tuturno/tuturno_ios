//
//  AppDelegate.m
//  TuTurno
//
//  Created by julian david rua pino on 24/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
//#import "SBJson.h"
#import "InfoTurnosViewController.h"
#import "NegocioTurno.h"
//#import "Services.h"
#import "NegocioTurno.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    sleep(2);
    
    
    
    
    
    //-- Set Notification
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    //--- your custom code
    
    
    
    UIRemoteNotificationType enabledTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    UIRemoteNotificationType tipe = enabledTypes;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
   
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [NegocioTurno deactivateNotificatoins];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [NegocioTurno activateNotifications];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    
    
    NSString *device = [[[deviceToken description]
                         stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                        stringByReplacingOccurrencesOfString:@" "
                        withString:@""];
    
    //se guarda en memoria el devicetoken
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:device forKey:@"devicetoken"];
    
    [defaults synchronize];
    
    
    NSLog(@"My token is: %@", device);
    
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    
    UIViewController *pVC = self.window.rootViewController;
    
    NSDictionary *infoqueue = [[NSDictionary alloc] initWithObjectsAndKeys:[userInfo objectForKey:@"queue"], @"queue", nil];
    NSDictionary *infotype = [[NSDictionary alloc] initWithObjectsAndKeys:[userInfo objectForKey:@"type"], @"type", nil];
    
    
    //int turno = [[infonumber valueForKey:@"number"] intValue];
    int idd = [[infoqueue valueForKey:@"queue"] intValue];
    NSString *type = infotype[@"type"];
    
    // se guarda en memoria el usuario
    NSUserDefaults *defaultss = [NSUserDefaults standardUserDefaults];
    
    [defaultss setObject:[NSString stringWithFormat:@"%d",idd ] forKey:@"queueId"];
  //  [defaultss setObject:[NSString stringWithFormat:@"%d",turno ] forKey:@"Turno"];
    
    NegocioTurno *Nturno = [[NegocioTurno alloc]init];
    
   Boolean sw =  Nturno.ConsultarTurno;
    
    Boolean swtuturno = false;
 
     // int  turno = [[defaultss objectForKey:@"Turno"] intValue];
     // int  turnoactual = [[defaultss objectForKey:@"TurnoActual"] intValue];
    
    if([type isEqualToString:@"YourTurnCall"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil];
        NSString *segueId = @"Home-TuTurno";
        [self.window.rootViewController performSegueWithIdentifier:segueId sender:self];
            
    } else if(([type isEqualToString:@"TurnCall"]))
    {
       [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil];
    }
    else if([type isEqualToString:@"NumericTurnRegistration"])
    {
        NSString *segueId = @"InfoTurno";
        [self.window.rootViewController performSegueWithIdentifier:segueId sender:self];
    }
    
   
        
    
    
}

-(NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    // return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
}

-(NSString *)gmthoras
{
    //datetime actual
    NSTimeInterval seconds; // assume this exists
    NSDate* ts_utc = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"EST"]];
    [df_local setDateFormat:@"zzz"];
    
    NSString* ts_local_string = [df_local stringFromDate:ts_utc];
    
    NSArray *array = [ts_local_string componentsSeparatedByString:@"GMT"];
    
    NSString *GMT = array[1];
    
    //NSInteger horas = [GMT intValue];
    
    // horas = horas * -1;
    
    //return &horas;
    
    return GMT;
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
}

-(void)crearnotificacionlocal :(NSDate *) fechamostrar :(NSInteger *) minutos
{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    //  NSDate *currentDate = [NSDate date];
    NSDate *datePlusOneMinute = [fechamostrar dateByAddingTimeInterval:*minutos];
    
    localNotification.fireDate = datePlusOneMinute;
    localNotification.alertBody = @"Alertaaa!!!!";//[NSString stringWithFormat:@"Alert Fired at %@", datePlusOneMinute];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    //localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(void)confirmarpush:(NSString *) uuid
{
    //servicio post
    
    NSString *post = [NSString stringWithFormat:@"{\"uuid\":\"%@\"}",uuid];
    //NSString *post = [NSString stringWithFormat:@"%@%@",@"uuid=",uuid];
    // NSString *post = @"Leng=es";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //NSData *postData = [NSData dataWithBytes:[post UTF8String] length:[post UTF8String]];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/notification/acknowledge.json"]]];
    //[request setURL:[NSURL URLWithString:@"http://apimundialareamovil.azurewebsites.net/api/grupos"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    if(conn) {
        NSLog(@"Connection Successful");
    } else {
        NSLog(@"Connection could not be made");
    }
    
    NSError *error;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
}

@end
