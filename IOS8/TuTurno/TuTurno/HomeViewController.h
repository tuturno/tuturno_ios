//
//  HomeViewController.h
//  TuTurno
//
//  Created by julian david rua pino on 28/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
- (IBAction)btnTurnosTouch:(id)sender;
- (IBAction)btnSoporteTouch:(id)sender;

@end
