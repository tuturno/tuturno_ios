//
//  HomeViewController.m
//  TuTurno
//
//  Created by julian david rua pino on 28/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "HomeViewController.h"
#import "Alert.h"
#import "InfoTurnosViewController.h"
#import "NegocioTurno.h"
#import "SVProgressHUD/SVProgressHUD.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    NSString *idusuario = [usuario objectForKey:@"idusuario"];
    
    if(idusuario == nil)
    {
        [self performSegueWithIdentifier:@"HomeRegistro" sender:self];
        // viewCelular.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnTurnosTouch:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Cargando..." maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
    
    NegocioTurno *Nturno = [[NegocioTurno alloc]init];
    
    Boolean sw = Nturno.ConsultarTurno;
    
    if(sw)
    {
        //InfoTurno
        [self performSegueWithIdentifier:@"InfoTurno" sender:self];
    }
    else
    {
        [Alert mostrar:@"No tienes turnos asignados"];
        //[self performSegueWithIdentifier:@"InfoTurno" sender:self];
    }
        
    
            [SVProgressHUD dismiss];
        });
    });
    
}

- (IBAction)btnSoporteTouch:(id)sender {
    
    //http://www.tuturnoapp.com/ayuda/
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tuturnoapp.com/ayuda/"]];
}
@end
