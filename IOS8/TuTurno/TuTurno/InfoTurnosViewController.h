//
//  InfoTurnosViewController.h
//  TuTurno
//
//  Created by julian david rua pino on 28/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoTurnosViewController : UIViewController
{
    NSArray *DatosArray;
}

@property (weak, nonatomic) IBOutlet UILabel *lbTurnoActual;
@property (weak, nonatomic) IBOutlet UILabel *lbTuTurno;
@property (weak, nonatomic) IBOutlet UILabel *lbTiempoEstimado;
- (IBAction)touch:(id)sender;
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@end
