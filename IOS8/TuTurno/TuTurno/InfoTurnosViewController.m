//
//  InfoTurnosViewController.m
//  TuTurno
//
//  Created by julian david rua pino on 28/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "InfoTurnosViewController.h"
#import "Services.h"
#import "NegocioTurno.h"
#import "SVProgressHUD/SVProgressHUD.h"

@interface InfoTurnosViewController ()

@end

@implementation InfoTurnosViewController

static int turno;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: @"http://webcontrol.tuturnoapp.com/images/santafe-logo-width.png"]];
   // UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageWithData: imageData]];
    
    self.image.image = [UIImage imageWithData:imageData];
    
    [NegocioTurno activateNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:@"refreshView" object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [SVProgressHUD showWithStatus:@"Cargando..." maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
    [self actualizarturno];
            
            [SVProgressHUD dismiss];
        });
    });
}

-(void)refreshView:(NSNotification *) notification
{
    [self actualizarturno];

  // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshView" object:nil];
}
-(void)actualizarturno
{
  //  NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
  //  [usuario setObject:@"NO" forKey:@"swInfo"];
   // [usuario synchronize];
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    //NSString *idusuario = [usuario objectForKey:@"idusuario"];
    NSString *IdQueue = [usuario objectForKey:@"queueId"];
    turno = [[usuario objectForKey:@"Turno"] intValue];
    
    
    Services *Nservices = [[Services alloc]init];
    
    NSError *error;
    
    NSString *url = [NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/numericQueue/show/%@.json",IdQueue];
    
    NSDictionary *resultado = [Nservices ObtenerDatosDictionary:url Data:nil Type:0 error:&error];
    
    
  //  long idpush = [[resultado objectForKey:@"currentTurn"] longValue];
  //  long lastTurnCallDatetime = [[resultado objectForKey:@"lastTurnCallDatetime"] longValue];
    long lastAssignedTurn = [[resultado objectForKey:@"lastAssignedTurn"] longValue];
    long maxTurn = [[resultado objectForKey:@"maxTurn"] longValue];
    long currentTurn = [[resultado objectForKey:@"currentTurn"] longValue];
    long turnCallsTimeLapseAverage = [[resultado objectForKey:@"turnCallsTimeLapseAverage"] longValue];
    
   // self.lbTuTurno.text = [NSString stringWithFormat:@"%ld", lastAssignedTurn];
    self.lbTuTurno.text = [NSString stringWithFormat:@"%d",turno];
    self.lbTurnoActual.text = [NSString stringWithFormat:@"%ld",currentTurn];
    
    NegocioTurno *Nturno = [[NegocioTurno alloc]init];
    
    self.lbTiempoEstimado.text = [Nturno tiempoEstimado:turnCallsTimeLapseAverage :maxTurn :currentTurn :lastAssignedTurn];
    

    [usuario setObject:self.lbTurnoActual.text forKey:@"TurnoActual"];
    
    if([self.lbTuTurno.text isEqualToString:self.lbTurnoActual.text])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
         [self performSegueWithIdentifier:@"tuturno" sender:self];
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touch:(id)sender {
    
    [NegocioTurno deactivateNotificatoins];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
 //  [self performSegueWithIdentifier:@"InfoHome" sender:self];
}

- (IBAction)btnBack:(id)sender {
    
    [NegocioTurno deactivateNotificatoins];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
