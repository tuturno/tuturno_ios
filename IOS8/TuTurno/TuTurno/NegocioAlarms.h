//
//  NegocioAlarms.h
//  TuTurno
//
//  Created by julian david rua pino on 6/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NegocioAlarms : NSObject

-(NSArray *)getAlarms;
-(void)alarmDisable:(int)Id;
-(void)alarmEnable:(int)Id;

@end
