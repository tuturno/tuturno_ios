//
//  NegocioAlarms.m
//  TuTurno
//
//  Created by julian david rua pino on 6/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "NegocioAlarms.h"
#import "Services.h"

@implementation NegocioAlarms

-(NSArray *)getAlarms
{
    Services *Nservices = [[Services alloc]init];
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];

    
    NSString *url = [NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/alarm/listByTurn.json?turn=%@",[usuario objectForKey:@"Turno"]];
    
    NSError *error;
    
    NSArray *datos = [Nservices ObtenerDatos:url Data:nil Type:0 error:&error];
    
    return datos;
}

-(void)alarmEnable:(int)Id
{
    NSString *post = [NSString stringWithFormat:@"{\"alarm\":\"%d\"}",Id];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    Services *Nservices = [[Services alloc]init];
    
    NSString *url = @"http://tuturno.elasticbeanstalk.com/alarm/enable.json";
    
    NSError *error;
    
    NSArray *datos = [Nservices ObtenerDatos:url Data:postData Type:1 error:&error];

}

-(void)alarmDisable:(int)Id
{
    NSString *post = [NSString stringWithFormat:@"{\"alarm\":\"%d\"}",Id];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    Services *Nservices = [[Services alloc]init];
    
    NSString *url = @"http://tuturno.elasticbeanstalk.com/alarm/disable.json";
    
    NSError *error;
    
    NSArray *datos = [Nservices ObtenerDatos:url Data:postData Type:1 error:&error];
}

@end
