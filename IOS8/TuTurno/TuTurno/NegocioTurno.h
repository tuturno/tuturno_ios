//
//  NegocioTurno.h
//  TuTurno
//
//  Created by julian david rua pino on 5/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NegocioTurno : NSObject

-(NSString *) tiempoEstimado:(long) turnCallsTimeLapseAverage :(long) maxTurn :(long) currentTurn :(long) lastAssignedTurn;
-(NSString *)stringFromTimeInterval:(NSTimeInterval)interval;
+ (void) activateNotifications;
+ (void) deactivateNotificatoins;
-(Boolean) ConsultarTurno;


@end
