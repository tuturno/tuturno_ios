//
//  RegistroViewController.h
//  TuTurnoApp
//
//  Created by Julián Rúa on 16/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistroViewController : UIViewController
- (IBAction)btnRegistro:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtcelular;
- (IBAction)txtcelularChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMensaje;
- (IBAction)txtcelularEditingBegin:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRegistroOutlet;

@end
