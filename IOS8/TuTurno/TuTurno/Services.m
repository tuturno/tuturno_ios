//
//  Services.m
//  Simple Mobile
//
//  Created by Bernardo Restrepo Molina on 13/08/14.
//  Copyright (c) 2014 Nexos Software S.A.S. All rights reserved.
//

#import "Services.h"

@implementation Services

static NSError *ut_make_error( int code, NSString *domain, NSString *description )
{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:description forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:domain code:code userInfo:errorDetail];
}

static Services *sharedInstance = nil;
+(id)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

-(NSArray *) ObtenerDatos:(NSString *)strUrl Data:(NSData *)postData Type:(NSInteger *)type error:(NSError **) err{
    NSString *strURLJson = strUrl;
	
	NSError* error = nil;
	NSURLResponse* response = nil;
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
	
	NSURL* URL = [NSURL URLWithString:strURLJson];
	[request setURL:URL];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	[request setTimeoutInterval:10];
    
    if(type == 0){
        [request setHTTPMethod:@"GET"];
    }
    else {
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
    }
    
	NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
	if (error)
	{
        *err = ut_make_error(0, NSLocalizedString(@"APPLICATION", nil), NSLocalizedString(@"TEXT_ERROR_SERVICE", nil));
        return nil;
	}
    
	/*NSMutableArray *arrResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    for (NSMutableDictionary *dic in arrResult)
    {
        NSString *string = dic[@"array"];
        if (string)
        {
            NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
            dic[@"array"] = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        else
        {
            NSLog(@"Error in url response");
        }
    }*/
    
	NSArray *arrResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    return arrResult;
}

-(NSDictionary *) ObtenerDatosDictionary:(NSString *)strUrl Data:(NSData *)postData Type:(NSInteger *)type error:(NSError **) err{
    NSString *strURLJson = strUrl;
    
    NSError* error = nil;
    NSURLResponse* response = nil;
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    NSURL* URL = [NSURL URLWithString:strURLJson];
    [request setURL:URL];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:10];
    
    if(type == 0){
        [request setHTTPMethod:@"GET"];
    }
    else {
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
    }
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        *err = ut_make_error(0, NSLocalizedString(@"APPLICATION", nil), NSLocalizedString(@"TEXT_ERROR_SERVICE", nil));
        return nil;
    }
    
    /*NSMutableArray *arrResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
     
     for (NSMutableDictionary *dic in arrResult)
     {
     NSString *string = dic[@"array"];
     if (string)
     {
     NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
     dic[@"array"] = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
     }
     else
     {
     NSLog(@"Error in url response");
     }
     }*/
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    return result;
}




@end
