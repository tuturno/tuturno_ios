//
//  ViewController.h
//  TuTurno
//
//  Created by Julián Rúa on 5/05/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelhora;
@property (weak, nonatomic) IBOutlet UIView *lbhora;
@property (weak, nonatomic) IBOutlet UILabel *labeltiempo;
@property (nonatomic,strong) NSString *hora;
@property (weak, nonatomic) IBOutlet UILabel *labelfecha;
@property (weak, nonatomic) IBOutlet UIView *viewCelular;
@property (weak, nonatomic) IBOutlet UITextField *txtcelular;
- (IBAction)btnregistrarse:(id)sender;
- (IBAction)txtcelularChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *viewfinal;
@property (weak, nonatomic) IBOutlet UIImageView *viewinicial;

@end
