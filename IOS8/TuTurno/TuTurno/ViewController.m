//
//  ViewController.m
//  TuTurno
//
//  Created by Julián Rúa on 5/05/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
//#import "PantallasViewController.h"

//#import "InformativoViewController.h"


@interface ViewController ()

@end

@implementation ViewController

@synthesize labelhora;
@synthesize labeltiempo;
@synthesize labelfecha;
@synthesize viewCelular;
@synthesize txtcelular;
@synthesize viewfinal,viewinicial;



- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // for (NSString* family in [UIFont familyNames])
   // {
     //   NSLog(@"%@", family);
        
     //   for (NSString* name in [UIFont fontNamesForFamilyName: family])
      //  {
      //      NSLog(@"  %@", name);
      //  }
   // }

    
    
    //consultar si el usuario ya esta registrado
    
    // Get the stored data before the view loads
   
	// Do any additional setup after loading the view, typically from a nib.
    
    //self.labelhora.text =
    
   // labelhora.text = @"prueba";
    
    //alarma de prueba
    
    
  //  UIStoryboard *mainstori = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  //  UIViewController *vc = [mainstori instantiateViewControllerWithIdentifier:@"informativo"];
  //7  [self presentViewController:vc animated:YES completion:nil];
    
    
  
}


-(void) viewDidAppear:(BOOL)animated{
    
   // [self performSegueWithIdentifier:@"pantallas" sender:self];
    
    [txtcelular setDelegate:self];
    
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    NSString *idusuario = [usuario objectForKey:@"idusuario"];
    
    if(idusuario == nil)
    {
        [self performSegueWithIdentifier:@"registro" sender:self];
       // viewCelular.hidden = NO;
    }
    
    
    
    [self ActualizarServicio];
    
   
    
    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(onTick:)];
    NSInvocation *inv = [NSInvocation invocationWithMethodSignature: sgn];
    [inv setTarget: self];
    [inv setSelector:@selector(onTick:)];
    
    NSTimer *t = [NSTimer timerWithTimeInterval: 60
                                     invocation:inv
                                        repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer: t forMode: NSDefaultRunLoopMode];
    
    
    labeltiempo.font = [UIFont fontWithName:@"Digital-7Mono" size:40];
    labelhora.font = [UIFont fontWithName:@"Digital-7Mono" size:40];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
  //  AppDelegate *delegate = [segue destinationViewController];
    
  //  self.labelhora.text = delegate.hora;
    
}

-(void) consultarServicio
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *fecha = [defaults objectForKey:@"fecha"];
    
    NSString *idusuario = [defaults objectForKey:@"idusuario"];
    
    if(fecha == nil)
    {
        NSString *rurl = [NSString stringWithFormat:@"%@%@%@",@"http://tuturno.elasticbeanstalk.com/turn/listBySubscriberAndStatus.json?subscriber=",idusuario,@"&status=pending"];
        
        NSURL *url = [NSURL URLWithString:rurl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 // NSDictionary *parseobject = [NSJSONSerialization JSONObjectWithData:returnData options:nil error:&error];
                 NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:0
                                                                            error:NULL];
                 
                 for(NSDictionary *item in greeting) {
                     NSLog(@"Item: %@", item);
                     
                     NSString *fecha = [item objectForKey:@"datetime"];
                     
                     // se guarda en memoria la ultima fecha consultada
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:fecha forKey:@"fecha"];
                     
                 }
             }
         }];
        
    }
}

-(void) ActualizarServicio
{
    
    NSLog(@"se ejecuta");
    // Get the stored data before the view loads
    
    [self consultarServicio];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *fecha = [defaults objectForKey:@"fecha"];
    
    
    if (fecha != nil) {
        
        [self pantallas];
        
        double unixTimeStamp = [fecha doubleValue];
        NSTimeInterval _interval=unixTimeStamp;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setLocale:[NSLocale currentLocale]];
        [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *_date=[_formatter stringFromDate:date];
        
        NSDateFormatter *_mostrar = [[NSDateFormatter alloc]init];
        [_mostrar setLocale:[NSLocale currentLocale]];
        [_mostrar setDateFormat:@"dd - MM - yyyy"];
        
        
        self.labelfecha.text = [_mostrar stringFromDate:date];
        
       // NSString *GMT = [self gmthoras];
        
       // NSInteger horas = [GMT intValue];
        
      //  horas = horas * -1;
      
        //date = [date dateByAddingTimeInterval:horas*60];
        
        
        
        NSDate *fechaActual = [NSDate date];

        
        NSString *_dateactual = [_formatter stringFromDate:fechaActual];
        
        NSDate *actual = [_formatter dateFromString:_dateactual];
        NSDate *final = [_formatter dateFromString:_date];
        
        
        NSTimeInterval timedifference = [final timeIntervalSinceDate:actual]; /// 60.0;
        
        //si a pasado mas de una hora -1 se borra la memoria de la ultima fecha
        if ((timedifference / 3600) < 0) {
            // Store the data
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:nil forKey:@"fecha"];
            
            [defaults synchronize];
            
         //   [defaults setObject:nil forKey:@"alert"];
            
         //   [defaults synchronize];
            
            self.labeltiempo.text = @"00:00";
            self.labelhora.text = @"00:00";
            self.labelfecha.text = @"       Fecha";
            
            // se guarda en memoria
            
            
            NSLog(@"Data saved");
        }
        else
        {
            
            
            NSDateFormatter *mostrar=[[NSDateFormatter alloc]init];
            [mostrar setLocale:[NSLocale currentLocale]];
            [mostrar setDateFormat:@"HH:mm"];
            
            self.labelhora.text = [mostrar stringFromDate:date];;
            self.labeltiempo.text = [self stringFromTimeInterval:timedifference];
            
            //int alert = [defaults objectForKey:@"alert"];
            
          //  if(alert != 0)
          //  {
            NSInteger minn = (NSInteger)0;//-900;
            [self crearnotificacionlocal:final :&minn];
            
            NSInteger min = (NSInteger)-900;//-900;
            [self crearnotificacionlocal:final :&min];
            
            NSInteger minfinal = (NSInteger)3600;
            
            //configurar pantalla viwefinal
            NSDate *datePlusOneMinute = [final dateByAddingTimeInterval:minfinal];
            // se guarda en memoria
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *_date=[_formatter stringFromDate:datePlusOneMinute];
            
            [defaults setObject:_date forKey:@"fechaMostrarViwefinal"];
            
            
          //  [defaults setObject:0 forKey:@"alert"];
            
          //  [defaults synchronize];
          //  }
            
        }
        
    }
    else
    {
        [self pantallas];
    }

}


//timer
-(void)onTick:(NSTimer *)timer {
    
    //metodo que se ejecuta con el timer.
    
    [self ActualizarServicio];
    
    }

-(void)crearnotificacionlocal :(NSDate *) fechamostrar :(NSInteger *) minutos
{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
  //  NSDate *currentDate = [NSDate date];
    NSDate *datePlusOneMinute = [fechamostrar dateByAddingTimeInterval:*minutos];
    
    localNotification.fireDate = datePlusOneMinute;
    localNotification.alertBody = @"Alertaaa!!!!";//[NSString stringWithFormat:@"Alert Fired at %@", datePlusOneMinute];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    //localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(void)pantallas
{
    //consultar fecha de turno
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fecha = [defaults objectForKey:@"fecha"];
    
    
    double unixTimeStamp = [fecha doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *fechaTurno = [NSDate dateWithTimeIntervalSince1970:_interval];
    
    //fecha actual
    NSDate *fechaActual = [NSDate date];
    
    //fecha para mostrar viewfinal
    NSString *fechafinal = [defaults objectForKey:@"fechaMostrarViwefinal"];
    
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setLocale:[NSLocale currentLocale]];
    [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSDate *fechamostrar = [_formatter dateFromString:fechafinal];
    
    //double unixTimeStampfinal = [fechafinal doubleValue];
    //NSTimeInterval _intervalfinal=unixTimeStampfinal;
    //NSDate *fechamostrar = [NSDate dateWithTimeIntervalSince1970:_intervalfinal];
    
    NSDate *pasadahora = [fechamostrar dateByAddingTimeInterval:3600];
    
    
    if(fecha == nil && pasadahora > fechaActual )
    {
        //viewfinal.hidden = NO;
        
       // viewpantallas.pantalla = 1;
         [defaults setObject:@"1" forKey:@"pantalla"];
        [self performSegueWithIdentifier:@"pantallas" sender:self];
        //viewinicial.hidden=YES;
    }
    else
    {
        
         //viewfinal.hidden = YES;
       // viewinicial.hidden=NO;
        
        if(fecha == nil)
        {
            //viewpantallas.pantalla = 2;
            [defaults setObject:@"2" forKey:@"pantalla"];
            [self performSegueWithIdentifier:@"pantallas" sender:self];
           // viewinicial.hidden = NO;
        }
        else
        {
           // viewinicial.hidden = YES;
        }
    }
    
    
   // [defaults synchronize];
    
    //registrar equipo
    
  //  NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
  //  NSString *idusuario = [usuario objectForKey:@"idusuario"];
    
  //  if(idusuario == nil)
  //  {
   //     viewCelular.hidden = NO;
    //    viewfinal.hidden=YES;
    //    viewinicial.hidden=YES;
  //  }
  //  else
  //  {
   //     viewCelular.hidden = YES;
  //  }
   
    
}

-(NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    // return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
}

-(NSString *)gmthoras
{
    //datetime actual
    NSTimeInterval seconds; // assume this exists
    NSDate* ts_utc = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"EST"]];
    [df_local setDateFormat:@"zzz"];
    
    NSString* ts_local_string = [df_local stringFromDate:ts_utc];
    
    NSArray *array = [ts_local_string componentsSeparatedByString:@"GMT"];
    
    NSString *GMT = array[1];
    
    //NSInteger horas = [GMT intValue];
    
    // horas = horas * -1;
    
    //return &horas;
    
    return GMT;
}

- (IBAction)btnregistrarse:(id)sender {
    

    
}


- (IBAction)txtcelularChanged:(id)sender {
    
    
}

@end
