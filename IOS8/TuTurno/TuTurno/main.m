    //
//  main.m
//  TuTurno
//
//  Created by julian david rua pino on 24/09/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
