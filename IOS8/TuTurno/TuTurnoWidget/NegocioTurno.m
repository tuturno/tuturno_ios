//
//  NegocioTurno.m
//  TuTurno
//
//  Created by julian david rua pino on 5/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "NegocioTurno.h"
#import "Services.h"
#import "SVProgressHUD.h"

@implementation NegocioTurno

-(Boolean) ConsultarTurno
{
    
    Services *Nservices = [[Services alloc]init];
    
    NSError *error;
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    NSString *idusuario = [usuario objectForKey:@"idusuario"];
    
    NSString *Url = [NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/turn/listBySubscriberAndStatus.json?subscriber=%@&status=%@",idusuario,@"pending"];
    
    NSArray *datos = [Nservices ObtenerDatos:Url Data:nil Type:0 error:&error];
    
    Boolean sw = false;
    
    if([datos count] > 0)
    {
        
        NSDictionary *resultados = datos[0];
        NSDictionary *queue = [resultados objectForKey:@"queue"];
        // NSDictionary *number = [resultados objectForKey:@"number"];
        int idd = [queue[@"id"] intValue];
        int turno = [resultados[@"number"]intValue];
        
        // se guarda en memoria el usuario
        NSUserDefaults *defaultss = [NSUserDefaults standardUserDefaults];
        
        [defaultss setObject:[NSString stringWithFormat:@"%d",idd ] forKey:@"queueId"];
        [defaultss setObject:[NSString stringWithFormat:@"%d",turno ] forKey:@"Turno"];
        
        [defaultss synchronize];
        //NSDictionary *resultados = [self indexKeyedDictionaryFromArray:queue];
        
        //InfoTurno
        sw = true;
    }
    
    return sw;

}

-(NSString *) tiempoEstimado:(long) turnCallsTimeLapseAverage :(long) maxTurn :(long) currentTurn :(long) lastAssignedTurn
{
    
    
    NSInteger resultado = lastAssignedTurn - currentTurn;
    
    NSInteger tiempo = resultado * turnCallsTimeLapseAverage;
    
    return [self stringFromTimeInterval:tiempo];
}

-(NSString *)stringFromTimeInterval:(NSTimeInterval)interval
{
    
    NSInteger Seconds;
    NSInteger Minutes;
    NSInteger Hours;
    
    //Find The Seconds
    Seconds = interval;
    
    //Find The Minutes
    NSInteger result = interval/60 ;
    Minutes = result % 60;
    
    //Find The Hours
    Hours = (interval / 60)/60;
    
    // NSInteger ti = (NSInteger)interval;
    //  NSInteger seconds = ti % 60;
    //  NSInteger minutes = (ti / 60) % 60;
    //  NSInteger hours = (ti / 3600);
    // return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)Hours, (long)Minutes];
}

//convierte una Array en Dictionary
+ (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
    id objectInstance;
    NSUInteger indexKey = 0U;
    
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    for (objectInstance in array)
        [mutableDictionary setObject:objectInstance forKey:[NSNumber numberWithUnsignedInt:indexKey++]];
    
    return (NSDictionary *)mutableDictionary;
}

+ (void)activateNotifications
{
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    NSString *subscriber = [usuario objectForKey:@"idusuario"];
    
    NSString *post = [NSString stringWithFormat:@"{\"subscriber\":\"%@\"}",subscriber];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *url = @"http://tuturno.elasticbeanstalk.com/subscriber/activateNotifications.json";
    NSError *error;
    
    Services *servicios = [[Services alloc]init];
    NSArray *resultado = [servicios ObtenerDatos:url Data:postData Type:1 error:&error];
    
}

+ (void)deactivateNotificatoins
{
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    NSString *subscriber = [usuario objectForKey:@"idusuario"];
    
    NSString *post = [NSString stringWithFormat:@"{\"subscriber\":\"%@\"}",subscriber];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *url = @"http://tuturno.elasticbeanstalk.com/subscriber/deactivateNotifications.json";
    NSError *error;
    
    Services *servicios = [[Services alloc]init];
    NSArray *resultado = [servicios ObtenerDatos:url Data:postData Type:1 error:&error];
    
}

@end
