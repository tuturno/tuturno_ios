//
//  Services.h
//  Simple Mobile
//
//  Created by Bernardo Restrepo Molina on 13/08/14.
//  Copyright (c) 2014 Nexos Software S.A.S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Services : NSObject

@property (nonatomic, assign) NSInteger *networkingCount;

+(id)sharedInstance;
-(NSArray *) ObtenerDatos:(NSString *)strUrl Data:(NSData *)data Type:(NSInteger *)type error:(NSError **) err;
-(NSDictionary *) ObtenerDatosDictionary:(NSString *)strUrl Data:(NSData *)postData Type:(NSInteger *)type error:(NSError **) err;
@end
