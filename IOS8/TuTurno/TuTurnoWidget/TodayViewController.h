//
//  TodayViewController.h
//  TuTurnoWidget
//
//  Created by julian david rua pino on 25/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTurnoActual;
@property (weak, nonatomic) IBOutlet UILabel *labelTuTurno;
@end
