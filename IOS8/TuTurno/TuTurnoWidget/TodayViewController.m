//
//  TodayViewController.m
//  TuTurnoWidget
//
//  Created by julian david rua pino on 25/10/14.
//  Copyright (c) 2014 SixTiGroup S.A.S. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "NegocioTurno.h"
#import "Services.h"

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    
    self.labelTurnoActual.text = @"Proximamente";
    
    return;
    //prueba
    
    NSUserDefaults *usuario = [[NSUserDefaults alloc] initWithSuiteName:@"SixTiGroup.TuTurno"];
    
    NSString *idusuario = [usuario objectForKey:@"idusuario"];
    
    //NSString *idusuario = [usuario objectForKey:@"idusuario"];
    NSString *IdQueue = [usuario objectForKey:@"queueId"];
   int turno = [[usuario objectForKey:@"Turno"] intValue];
    
    
    Services *Nservices = [[Services alloc]init];
    
    NSError *error;
    
    NSString *url = [NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/numericQueue/show/%@.json",IdQueue];
    
    NSDictionary *resultado = [Nservices ObtenerDatosDictionary:url Data:nil Type:0 error:&error];
    
    
    //  long idpush = [[resultado objectForKey:@"currentTurn"] longValue];
    //  long lastTurnCallDatetime = [[resultado objectForKey:@"lastTurnCallDatetime"] longValue];
    long lastAssignedTurn = [[resultado objectForKey:@"lastAssignedTurn"] longValue];
    long maxTurn = [[resultado objectForKey:@"maxTurn"] longValue];
    long currentTurn = [[resultado objectForKey:@"currentTurn"] longValue];
    long turnCallsTimeLapseAverage = [[resultado objectForKey:@"turnCallsTimeLapseAverage"] longValue];
    
    // self.lbTuTurno.text = [NSString stringWithFormat:@"%ld", lastAssignedTurn];
    self.labelTuTurno.text = [NSString stringWithFormat:@"%d",turno];
    self.labelTurnoActual.text = [NSString stringWithFormat:@"%ld",currentTurn];
    
    NegocioTurno *Nturno = [[NegocioTurno alloc]init];
    
    //self.lbTiempoEstimado.text = [Nturno tiempoEstimado:turnCallsTimeLapseAverage :maxTurn :currentTurn :lastAssignedTurn];
    
    
//    [usuario setObject:self.lbTurnoActual.text forKey:@"TurnoActual"];
//    
//    if([self.lbTuTurno.text isEqualToString:self.lbTurnoActual.text])
//    {
//        [self dismissViewControllerAnimated:YES completion:nil];
//        [self performSegueWithIdentifier:@"tuturno" sender:self];
//    }
    
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

@end
