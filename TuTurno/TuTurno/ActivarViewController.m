//
//  ActivarViewController.m
//  TuTurnoApp
//
//  Created by Julián Rúa on 17/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import "ActivarViewController.h"

@interface ActivarViewController ()

@end

@implementation ActivarViewController

@synthesize txtcodigo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnregistrarse:(id)sender {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *idusuario = [defaults objectForKey:@"idusuario"];
    
    [self CogigoConfirmar:idusuario :txtcodigo.text];
    
    [self performSegueWithIdentifier:@"tiempo" sender:self];

}

-(void)CogigoConfirmar:(NSString *) subscriber:(NSString *) codigo
{
    //servicio post
    
    NSString *post = [NSString stringWithFormat:@"{\"subscriber\":\"%@\",\"code\":\"%@\"}",subscriber,codigo];
    //NSString *post = [NSString stringWithFormat:@"%@%@",@"uuid=",uuid];
    // NSString *post = @"Leng=es";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //NSData *postData = [NSData dataWithBytes:[post UTF8String] length:[post UTF8String]];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/subscriber/activate.json"]]];
    //[request setURL:[NSURL URLWithString:@"http://apimundialareamovil.azurewebsites.net/api/grupos"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    if(conn) {
        NSLog(@"Connection Successful");
    } else {
        NSLog(@"Connection could not be made");
    }
    
    NSError *error;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
   
    if (returnData.length > 0 && error == nil)
    {
        // NSDictionary *parseobject = [NSJSONSerialization JSONObjectWithData:returnData options:nil error:&error];
        NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:returnData
                                                                 options:0
                                                                   error:NULL];
        NSString *status = [greeting objectForKey:@"status"];
        
        if([status  isEqual: @"active"])
             {
              [self performSegueWithIdentifier:@"tiempo" sender:self];
             }
             else
             {
               [self performSegueWithIdentifier:@"celuar" sender:self];
             }
    }
    
}
@end
