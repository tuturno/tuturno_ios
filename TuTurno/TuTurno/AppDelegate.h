//
//  AppDelegate.h
//  TuTurno
//
//  Created by Julián Rúa on 5/05/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
