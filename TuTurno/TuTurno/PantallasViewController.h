//
//  PantallasViewController.h
//  TuTurnoApp
//
//  Created by Julián Rúa on 13/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PantallasViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageFinal;
@property (weak, nonatomic) IBOutlet UIImageView *imageEspera;

@end
