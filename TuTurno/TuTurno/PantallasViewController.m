//
//  PantallasViewController.m
//  TuTurnoApp
//
//  Created by Julián Rúa on 13/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import "PantallasViewController.h"

@interface PantallasViewController ()

@end

@implementation PantallasViewController

@synthesize imageEspera,imageFinal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *pantalla = [defaults objectForKey:@"pantalla"];
    
    
    
    if([pantalla  isEqual: @"1"])
    {
        imageFinal.hidden = NO;
        imageEspera.hidden = YES;
    }
    else
    {
        imageFinal.hidden = YES;
        imageEspera.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
