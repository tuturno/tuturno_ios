//
//  RegistroViewController.m
//  TuTurnoApp
//
//  Created by Julián Rúa on 16/08/14.
//  Copyright (c) 2014 com.tuturno.tuturno. All rights reserved.
//

#import "RegistroViewController.h"

@interface RegistroViewController ()

@end

@implementation RegistroViewController

@synthesize txtcelular;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [txtcelular setDelegate:self];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnRegistro:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *device = [defaults objectForKey:@"devicetoken"];
    
    //servicio post
    
    NSString *post = [NSString stringWithFormat:@"%@%@%@%@",@"phoneNumber=",txtcelular.text,@"&phoneOS=ios&phoneToken=",device];
    // NSString *post = @"Leng=es";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://tuturno.elasticbeanstalk.com/subscriber/save.json"]];
    // [request setURL:[NSURL URLWithString:@"http://apimundialareamovil.azurewebsites.net/api/grupos"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
   // NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *parseobject = [NSJSONSerialization JSONObjectWithData:returnData options:nil error:&error];
    
    long Id = [parseobject[@"id"] longValue];
    
    NSString *idusuario = [NSString stringWithFormat: @"%d", Id];
    
    // se guarda en memoria el usuario
    NSUserDefaults *defaultss = [NSUserDefaults standardUserDefaults];
    
    [defaultss setObject:idusuario forKey:@"idusuario"];
    
    [defaultss synchronize];
    
    [self CogigoActivacion:idusuario];
    
    //viewCelular.hidden = YES;
    [self performSegueWithIdentifier:@"activar" sender:self];
    
    
    //  NSDictionary* Subscriber = [returnString JSONValue];
    // NSMutableArray* result = [[NSMutableArray alloc]init];
    
    //  for (int i=0; i<Subscriber.count; i++) {
    
    
    //  NSDictionary* subscriberinf = Subscriber[i];
    //   NSString *prueba = subscriberinf[@"Nombre_es"];
    //   NSLog(prueba);
    //subscriber* sb = [[subscriber alloc]init];
    
    // sb.class = subscriberinf[@"class"];
    // sb.id = subscriberinf[@"id"];
    // sb.phoneToken = subscriberinf[@"phoneToken"];
    
    // }
    
}
- (IBAction)txtcelularChanged:(id)sender {
    NSError *error;
    
    if(txtcelular.text.length > 10)
    {
        txtcelular.text = @"";
    }
    
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:
                                   @"^[0-9]*\.?[0-9]+$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    if ([regex numberOfMatchesInString:txtcelular.text options:0 range:NSMakeRange(0, [txtcelular.text length])])
    {
        NSLog(@"Matched : %@",txtcelular.text);
    }
    else
    {
        txtcelular.text = @"";
        NSLog(@"Not Matched : %@",txtcelular.text);
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtcelular resignFirstResponder];
    return YES;
}

-(void)CogigoActivacion:(NSString *) subscriber
{
    //servicio post
    
    NSString *post = [NSString stringWithFormat:@"{\"subscriber\":\"%@\"}",subscriber];
    //NSString *post = [NSString stringWithFormat:@"%@%@",@"uuid=",uuid];
    // NSString *post = @"Leng=es";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //NSData *postData = [NSData dataWithBytes:[post UTF8String] length:[post UTF8String]];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tuturno.elasticbeanstalk.com/activationCode/generate.json"]]];
    //[request setURL:[NSURL URLWithString:@"http://apimundialareamovil.azurewebsites.net/api/grupos"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    if(conn) {
        NSLog(@"Connection Successful");
    } else {
        NSLog(@"Connection could not be made");
    }
    
    NSError *error;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
}

@end
