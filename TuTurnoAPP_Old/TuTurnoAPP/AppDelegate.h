//
//  AppDelegate.h
//  TuTurnoAPP
//
//  Created by Areamovil on 23/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
