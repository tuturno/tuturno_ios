//
//  BackgroundLayer.h
//  TuTurnoAPP
//
//  Created by Areamovil on 24/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface BackgroundLayer : NSObject

+ (CAGradientLayer *) greyGradient;
+ (CAGradientLayer *) bluegradient;

@end
