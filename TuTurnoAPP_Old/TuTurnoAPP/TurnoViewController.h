//
//  TurnoViewController.h
//  TuTurnoAPP
//
//  Created by Areamovil on 2/12/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TurnoViewController : UIViewController

@property (nonatomic,strong) NSString *numTurno;
@property (nonatomic,strong) NSString *barcode;
@property (weak, nonatomic) IBOutlet UILabel *lbturnoactual;
@property (weak, nonatomic) IBOutlet UILabel *lbtiemporest;
@property (weak, nonatomic) IBOutlet UILabel *lbtuturno;

@end
