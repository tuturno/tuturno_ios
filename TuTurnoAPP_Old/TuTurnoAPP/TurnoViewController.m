//
//  TurnoViewController.m
//  TuTurnoAPP
//
//  Created by Areamovil on 2/12/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import "TurnoViewController.h"
#import "AudioToolbox/AudioServices.h"

@interface TurnoViewController ()

@end

@implementation TurnoViewController
@synthesize lbtiemporest,lbturnoactual,lbtuturno;

@synthesize numTurno,barcode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(numTurno);
    [super viewDidLoad];
    
    lbtuturno.text = numTurno;
    
     //swt = YES;
    
      NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(onTick:)];
      NSInvocation *inv = [NSInvocation invocationWithMethodSignature: sgn];
      [inv setTarget: self];
      [inv setSelector:@selector(onTick:)];
    
     NSTimer *t = [NSTimer timerWithTimeInterval: 1.0
                 invocation:inv
                   repeats:YES];
    
     NSRunLoop *runner = [NSRunLoop currentRunLoop];
     [runner addTimer: t forMode: NSDefaultRunLoopMode];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onTick:(NSTimer *)timer {
    
    //metodo que se ejecuta con el timer.
    
   // if(swt)
   // {
        //do smth
        //NSLog(@"@%","pruebas");
        
        //http://www.tu-turno.co/queue/*wmed*llin/
    
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",@"http://www.tu-turno.co/queue/",barcode,@"/"];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        NSError *error;
        
        NSDictionary *json = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        //labelcompany.text = json[@"company"];
        
        //lbturnoactual.text = json[@"current"];
    
    
        int Turno = [numTurno intValue];
        
        int currenTurno = [json[@"current"] intValue];
        
        int time_average = [json[@"time_average"] intValue];
        
        int remainingTime = 0;
        
        remainingTime = ((Turno - currenTurno) * time_average)/60;
        
        lbtiemporest.text = [NSString stringWithFormat:@"%d%@", remainingTime,@" Min"];
        lbturnoactual.text = [NSString stringWithFormat:@"%d", currenTurno];
    
    //AudioServicesPlaySystemSound (1005);
    
    if(Turno == currenTurno)
    {
        AudioServicesPlaySystemSound (1005);
        
    }
        
        
        //lbtiemporest.text = "";
        
        
    //}
}

@end
