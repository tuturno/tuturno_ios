//
//  UIImageViewAsync.h
//  TuTurnoAPP
//
//  Created by Areamovil on 24/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageViewAsync : UIImageView<NSURLConnectionDelegate>{
    NSURLConnection *imageConnection;
    NSMutableData *imageData;
}

-(void)loadFromUrl:(NSString*)url;
@end
