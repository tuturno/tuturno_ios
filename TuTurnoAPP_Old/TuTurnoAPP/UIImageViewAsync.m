//
//  UIImageViewAsync.m
//  TuTurnoAPP
//
//  Created by Areamovil on 24/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import "UIImageViewAsync.h"

@implementation UIImageViewAsync

-(void)loadFromUrl:(NSString*)url {
    
    // Datos de la imagen descargada
    imageData = [[NSMutableData alloc] init];
    
    // Creamos la URL
    NSURL* urlImage = [NSURL URLWithString:url];
    
    // Creamos la conexión de datos
    NSURLRequest *request = [NSURLRequest requestWithURL:urlImage
                                             cachePolicy:NSURLCacheStorageNotAllowed
                                         timeoutInterval:30.0];
    
    // Lanzamos la conexión
    imageConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

/**
 *  Recepción de datos asíncrona
 */
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [imageData appendData:data];
}

/**
 *  La conexión finaliza con error; imagen no descargada
 */
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    imageData = nil;
    imageConnection = nil;
}

/**
 *  Conexión finaliza con éxito; imagen descargada
 */
-(void)connectionDidFinishLoading:(NSURLConnection*)connection {
    
    [self setImage:[UIImage imageWithData:imageData]];
    imageData = nil;
    imageConnection = nil;
}

@end
