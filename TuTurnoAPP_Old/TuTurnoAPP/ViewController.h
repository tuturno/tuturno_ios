//
//  ViewController.h
//  TuTurnoAPP
//
//  Created by Areamovil on 23/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "TurnoViewController.h"

@interface ViewController : UIViewController <ZBarReaderDelegate,UITextFieldDelegate>
{
    TurnoViewController *viewTurno;
}
@property (weak, nonatomic) IBOutlet UILabel *labelcompany;
@property (weak, nonatomic) IBOutlet UIImageView *imagencomapy;
@property (weak, nonatomic) IBOutlet UITextField *lbturno;

- (IBAction)btturno:(id)sender;
-(BOOL)textFieldShouldReturn:(UITextField *)textField;


@end
