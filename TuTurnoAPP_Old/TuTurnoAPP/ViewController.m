//
//  ViewController.m
//  TuTurnoAPP
//
//  Created by Areamovil on 23/11/13.
//  Copyright (c) 2013 com.areamovil.TuTurno. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

@synthesize labelcompany;
@synthesize imagencomapy;
@synthesize lbturno;


bool *sw = YES,swt = NO;
NSString *barcode = @"";

- (void)viewDidLoad
{
    [super viewDidLoad];
    [lbturno setDelegate:self];
    
  // self.lbturno.text = @"20";
}
-(void)viewDidAppear:(BOOL)animated
{
    if(sw)
{
    sw = NO;
  [self capturar];
}
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [lbturno resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    //  get the decode results
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // just grab the first barcode
        break;
    
    // showing the result on textview
   // resultTextView.text = symbol.data;
    barcode = symbol.data;
    
   // resultImageView.image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // dismiss the controller
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    
    //llamar servicio company
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",@"http://www.tu-turno.co/queue/",barcode,@"/"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSError *error;
    
    NSDictionary *json = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    labelcompany.text = json[@"company"];
    
    NSLog(@"%@",json[@"company"]);
    
    //llamar servicio estilos
    
    NSString *urlStringcss = [NSString stringWithFormat:@"%@%@%@",@"http://www.tu-turno.co/queue/",barcode,@"/company/"];
    
    NSURL *urlcss = [NSURL URLWithString:urlStringcss];
    
    NSData *datacss = [NSData dataWithContentsOfURL:urlcss];
    
    NSError *errorcss;
    
    NSDictionary *jsoncss = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:datacss options:kNilOptions error:&errorcss];
    
    //labelcompany.text = jsoncss[@"company"];
    
    NSLog(@"%@",jsoncss[@"color_start"]);
    
   // NSString *color = jsoncss[@"color_start"];
    
    
    //seleccionar color fondo
    
    UIColor *colour;
    
    if([jsoncss[@"color_start"] isEqualToString:@"#6D3C71"])
    {
       colour = [[UIColor alloc]initWithRed:109.0/255.0 green:60.0/255.0 blue:113.0/255.0 alpha:1.0];
    }
    if([jsoncss[@"color_start"] isEqualToString:@"#0f0068"])
    {
       colour = [[UIColor alloc]initWithRed:15.0/255.0 green:0.0/255.0 blue:104.0/255.0 alpha:1.0];
    }
    
    
    //cambiar color fondo
    
    self.view.backgroundColor = colour;
    
    //url imagen
    
    NSString *urlimg = jsoncss[@"logo"];
    
    NSURL *imageURL = [NSURL URLWithString:urlimg];
    
    //cargar imagen
    
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    [imagencomapy setImage:[UIImage imageWithData:imageData]];
    
    
}
-(void) capturar
{
    NSLog(@"Scanning..");
   // resultTextView.text = @"Scanning..";
    
    ZBarReaderViewController *codeReader = [ZBarReaderViewController new];
    codeReader.readerDelegate=self;
    codeReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = codeReader.scanner;
    [scanner setSymbology: ZBAR_I25 config: ZBAR_CFG_ENABLE to: 0];
    
    [self presentViewController:codeReader animated:YES completion:nil];
}

- (IBAction)btturno:(id)sender {
    

    
    
    
    viewTurno = [TurnoViewController new];
    
    viewTurno.numTurno = lbturno.text;
    viewTurno.barcode = barcode;
    
    
    viewTurno.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:viewTurno animated:YES completion:nil];
    
    
}
@end
